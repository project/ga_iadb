<?php

namespace Drupal\google_analytics_iadb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure google_analytics_iadb settings for this site.
 */
class GoogleAnalyticsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_analytics_iadb_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['google_analytics_iadb.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_analytics_iadb.settings');

    $form['general'] = [
      '#markup' => '<label>If you need change these codes please contact KIC/ISU team</label>',
    ];


    $form['general']['google_analytics_production_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Analytics Production Code'),
      '#description' => $this->t('Add your Google Analytics code for production environment'),
      '#default_value' => $config->get('production_code'),
      '#placeholder' => 'UA-XXXXXXX-X',
      '#disabled' => isset($_ENV['PANTHEON_ENVIRONMENT']) && 'disabled'
    ];

    $form['general']['google_analytics_test_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Analytics Test Code'),
      '#description' => $this->t('Add your Google Analytics code for non production environments'),
      '#default_value' => $config->get('test_code'),
      '#placeholder' => 'UA-XXXXXXX-X',
      '#disabled' => 'disabled'
    ];

    isset($_ENV['PANTHEON_ENVIRONMENT']) && $form['actions']['#attributes']['class'][] = 'hidden';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->config('google_analytics_iadb.settings');
    $config
      // Set the submitted configuration setting.
      ->set('production_code', $form_state->getValue('google_analytics_production_code'))
      ->set('test_code', $form_state->getValue('google_analytics_test_code'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
